#!/bin/bash

# ignore zero byte files
if [ -s "$1" ]; then
	# capture both return code and stderr from nullcheck
	nullcheck_err="$(~/zfs_null_files/nullcheck "$1" 2>&1 >/dev/null)"
	nullcheck_retval=$?

	# if error, print msg to stderr and exit
	if [ -n "$nullcheck_err" ]; then
		1>&2 echo "$nullcheck_err"
		exit $nullcheck_retval
	# if null file, print its info
	elif [ $nullcheck_retval -eq 0 ]; then
		find "$1" -printf "%p,%A+,%C+,%T+,%s\n"
	fi
fi
