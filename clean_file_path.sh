#! /bin/bash

dirname=`dirname "$1"`
filename=`basename "$1"`

cd $dirname

while [ ! -d .zfs ]; do 
    cd ..
done

# Separated the zfs folder path from the subdirectory

zfsfolder="$PWD"

extfolder="${dirname:${#zfsfolder}}" 

cd .zfs/snapshot

snapshots=`ls -1 | grep daily|sort -r`

for snapshot in $snapshots; do
    if [ -f "${snapshot}${extfolder}/${filename}" ]; then
        ~/zfs_null_files/nullcheck "${snapshot}${extfolder}/${filename}" 
        if [ $? -eq 1 ]; then
            echo "$snapshot,${1}"
            exit 0
        fi
    fi
done  

echo "NONE: $1"
exit 1

