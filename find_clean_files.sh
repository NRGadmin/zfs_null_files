#!/bin/bash
#parallel_bin="/usr/bin/parallel"
parallel_bin="/opt/ozmt/bin/SunOS/parallel" # OmniOS location

# show program usage
show_usage() {
    echo
    echo "Usage: $0 [-j jobs] [-l joblog] [-r] {null_file_list}"
    echo "  [-j jobs] value to pass to --jobs parameter of parallel"
    echo "  [-l joblog] value to pass to --joblog parameter of parallel"
    echo "  [-r] pass --resume parameter to parallel, must use with -l"
    exit 1
}

# Minimum number of arguments needed by this program
MIN_ARGS=1

if [ "$#" -lt "$MIN_ARGS" ]; then
    show_usage
    exit 1
fi

pflag=
jflag=
lflag=
rflag=
fflag=
parallel_jobs=
parallel_joblog=
parallel_resume=

while getopts p:j:l:f:r opt; do
    case $opt in
        j)  # jobs params specified
            parallel_jobs="-j $OPTARG"
            jflag=1
            ;;
        l)  # joblog params specified
            parallel_joblog="--joblog $OPTARG"
            lflag=1
            ;;
        r)  # resume params specified
            parallel_resume="--resume"
            rflag=1
            ;;
        f)  # Follow file with output from PID
            follow_pid="$OPTARG"
            fflag=1
            ;;
        ?)  # Show program usage and exit
            show_usage
            exit 1
            ;;
        :)  # Mandatory arguments not specified
            echo "Option -$opt requires an argument."
            exit 1
            ;;
    esac
done

#Move to remaining arguments
shift $(($OPTIND - 1))
#echo "Remaining arguments are: $*"

# Quiet parallel citation notice
mkdir -p ~/.parallel
touch ~/.parallel/will-cite

(
if [ "$fflag" == '1' ]; then
    tail --pid=$follow_pid -c +1 -f $1 
else
    cat $1
fi
) | awk -F ',' '{print $1}' | $parallel_bin $parallel_jobs $parallel_joblog $parallel_resume ./clean_file_path.sh {}

