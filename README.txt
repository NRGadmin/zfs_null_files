Recommended usage:

./find_null_files.sh -o "-j 60" /Volume1 2>find_errors.log 1>find_files.log
./find_null_files.sh -o "-j 60" -p "-type d -name 'dev' -prune -o -type f -print" /HCPintraDB00 2>/HCPintraDB00/null_files/find_error_20200109.log 1>/HCPintraDB00/null_files/find_files_20200109.log
./find_null_files.sh -o "-j 60 --joblog /HCPintraDB00/null_files/joblog_20200109" -p "-type d -name 'dev' -prune -o -type f -print" /HCPintraDB00 2>/HCPintraDB00/null_files/find_error_20200109.log 1>/HCPintraDB00/null_files/find_files_20200109.log
time ./find_null_files.sh -o "-j 60 --joblog /HCPintraDB00/null_files/joblog_20200109" -p "-type d -name 'dev' -prune -o -type f -print" /HCPintraDB00 2>/HCPintraDB00/null_files/find_error_20200109.log 1>/HCPintraDB00/null_files/find_files_20200109.log

./find_clean_files.sh -j 10 -l /HCPintraDB00/null_files/find_clean_files_20200109.log /HCPintraDB00/null_files/find_files_20200109.log |tee /HCPintraDB00/null_files/clean_file_snapshots_20200109
