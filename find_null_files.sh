#!/bin/bash
#parallel_bin="/usr/bin/parallel"
parallel_bin="/opt/ozmt/bin/SunOS/parallel" # OmniOS location

# show program usage
show_usage() {
    echo
    echo "Usage: $0 [-p patterns] [-o parallelopts] {root path}"
    echo "  [-p patterns] pattern(s) to filter paths/files located by find, e.g. \"-name '*.gz' ! -name skip_file1\""
    echo "  [-o parallelopts] additional parameters to pass to parallel, e.g. \" -j 60\""
    exit 1
}

# Minimum number of arguments needed by this program
MIN_ARGS=1

if [ "$#" -lt "$MIN_ARGS" ]; then
    show_usage
    exit 1
fi

pflag=
oflag=
find_patterns=
parallel_opts=

while getopts p:o: opt; do
    case $opt in
        p)  # find patterns specified
            find_patterns="$OPTARG"
            pflag=1
            ;;
        o)  # parallel params specified
            parallel_opts="$OPTARG" 
            oflag=1
            ;;
        ?)  # Show program usage and exit
            show_usage
            exit 1
            ;;
        :)  # Mandatory arguments not specified
            echo "Option -$opt requires an argument."
            exit 1
            ;;
    esac
done

#Move to remaining arguments
shift $(($OPTIND - 1))
#echo "Remaining arguments are: $*"

# Quiet parallel citation notice
mkdir -p ~/.parallel
touch ~/.parallel/will-cite

find $1 $find_patterns -print0 | $parallel_bin --null $parallel_opts ./check_file.sh {}
